import { Injectable } from '@angular/core';
import * as io from "socket.io-client";
import {AlertController } from 'ionic-angular';
// import { DashboardPage } from '../../pages/dashboard/dashboard';

/*
  Generated class for the CorekProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CorekProvider {
  socketHost: string = "http://162.252.57.117:8095";
  // socketHost: string = "http://adminbj-proyectokamila.c9users.io:8082";
  
  socket: any;

  constructor(public alertCtrl: AlertController) {
    console.log('Hello CorekProvider Provider');
    // this.socket = io.connect(this.socketHost,{'reconnection':false});
    // this.conect();
  }

  public ConnectCorek()
  {
    this.socket = io.connect(this.socketHost,{'reconnection':false});
  }

  public login(datarequest,event)
  {
    let nf = Date.now().toString()+'createuser';

      this.socket.emit('get_users', {
        'event': nf,
        'key':'validemail',
        'condition':{
          'user_email': datarequest.email}
        
      }); 
    this.socket.on(nf, (data, key)=>{
      console.log(data[0]);
      let userdata = data[0];
      console.log(userdata);
      console.log(datarequest.email);
      console.log(datarequest.password  );
        this.socket.emit('signon' , {log:datarequest.email, pwd: datarequest.password,'event':event, 'key':{'key':'login', 'datauser':userdata}});
        // this.socket.on(event, (data,key)=>{
        //   console.log(data);
        //   if (data.token)
        //   {
        //     console.log("si");
        //   }
        //   else
        //   {
        //     let alert = this.alertCtrl.create({
        //       title: 'Error',
        //       subTitle: 'la contrasena no es correcta',
        //       buttons: ['OK']
        //     });
        //     alert.present();
        //   }
        // });

    });
  }

  public ConnectCorekconfig(nf){
    console.log('Fucnion conecta corek');
    this.socket = io.connect(this.socketHost,{'reconnection':true});// esto hace que se conecte al servidor (como cable directo)
    this.socket.on('connection', (data)=>{
      console.log('Emit Config');
      this.socket.emit('conf', { 'project': 'http://argos.com','event':nf});
    });
  }

  public recover_password(datarequest,event)
  {
    console.log(datarequest);
    this.socket.emit('confsmtp',{'key':'confsmtp','token':datarequest.token});
    this.socket.on('confsmtp', (data, key)=>{
      console.log(data);
      console.log(key);
      if(key == "confsmtp")
      {
        this.socket.emit('forgot_password',{"email":datarequest.email});
        this.socket.on("forgot_password", (data,key)=>{
          console.log(data);
          if (data.message == true)
          {
            let alert = this.alertCtrl.create({
              title: 'Solicitud de recuperación exitosa',
              subTitle: 'Hemos enviado un email. Revisalo y sigue los pasos.',
              buttons: ['OK']
            });
          alert.present();    
          }
          else
          {
            let alert = this.alertCtrl.create({
              title: 'Error',
              subTitle: 'El correo no existe',
              buttons: ['OK']
            });
          alert.present();    
          }
        });
      }
    });
  }
}
