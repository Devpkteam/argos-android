import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { Media, MediaObject } from '@ionic-native/media';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Geolocation } from '@ionic-native/geolocation';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the PermisosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-permisos',
  templateUrl: 'permisos.html',
})
export class PermisosPage {
  step =1;
  audio: MediaObject;
  filePath: string;
  fileName: string;
  constructor(public navCtrl: NavController, public navParams: NavParams , private camera: Camera , private media: Media ,  private file: File , private locationAccuracy: LocationAccuracy , private geolocation: Geolocation, private storage: Storage) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PermisosPage');
  }
  takePic(){
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation : true,
      targetWidth: 200,
      targetHeight: 200,
    }
    this.camera.getPicture(options).then((imageData) => {
        // imageData is either a base64 encoded string or a file URI
        // If it's base64:
          let image = 'data:image/jpeg;base64,' + imageData;
          

          }, (err) => {
            // Handle error
    });
    this.step = 3; 
  }
  selectPic(){
    const options: CameraOptions = {
      quality: 100,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation : true,
      targetWidth: 200,
      targetHeight: 200,
    }
    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
       
        console.log('enviar aqui imagen');
      }, (err) => {
        // Handle error
    });
    this.step = 3;
  }
  record(){
    this.fileName = 'record'+new Date().getDate()+new Date().getMonth()+new Date().getFullYear()+new Date().getHours()+new Date().getMinutes()+new Date().getSeconds()+'.3gp';
    this.filePath = this.file.externalDataDirectory.replace(/file:\/\//g, '') + this.fileName;
    this.audio = this.media.create(this.filePath);
    this.audio.startRecord();
    window.setTimeout(() => this.audio.stopRecord(), 3000);
    this.step =  4;
  }
  location(){
    
            this.geolocation.getCurrentPosition().then((resp) => {
              // resp.coords.latitude
              // resp.coords.longitude
             }).catch((error) => {
               console.log('Error getting location', error);
             });
         
    this.step = 5;
  }
  save(){
    this.storage.set("settings" , "true");
    this.navCtrl.pop();
  }
}
