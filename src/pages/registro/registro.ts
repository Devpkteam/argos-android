import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CorekProvider } from '../../providers/corek/corek';
// import { Storage } from '@ionic/storage';
import { AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';


/**
 * Generated class for the RegistroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-registro',
  templateUrl: 'registro.html',
})
export class RegistroPage {
  tipo;
  step = 1;
  nombre;
  telefono;
  cedula;
  email;
  password;
  residencia;
  idplaca;
  items: any[] = [];
  constructor(public alertCtrl: AlertController, public CorekProvider: CorekProvider,public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController) {
    this.tipo = this.navParams.get('rol');
    console.log(this.tipo);
  }

  ionViewDidEnter() 
  {
    console.log('enter detailpage');
    this.CorekProvider.ConnectCorek();
    this.CorekProvider.socket.on('connection', (data, key) => {
      let nf = Date.now().toString()+"confipri";
      this.CorekProvider.ConnectCorekconfig(nf);    
      this.CorekProvider.socket.on(nf, (data) => {
      this.Process();
      });
    });
  }

  Process()
  {  
    let condition ={
      'post_type': 'residencias',
      'post_autor':'publish'
    }
    this.CorekProvider.socket.emit('query_post',{'condition': condition});
    this.CorekProvider.socket.on("query_post", (data,key)=>{
      console.log(data);
      this.items = data;
    });  
  }    

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegistroPage');
  }

  next(){
    this.step = 2;
  }

  prev(){
    this.step = 1;
  }

  save()
  { 
    let loader = this.loadingCtrl.create({
      content: "Espere"
    });
    loader.present();
    console.log(this.residencia);
    console.log("registrando");
    this.CorekProvider.ConnectCorek();
    this.CorekProvider.socket.on('connection', (data, key) => {
      let nf = Date.now().toString()+"confipri";
      this.CorekProvider.ConnectCorekconfig(nf);    
      this.CorekProvider.socket.on(nf, (data) => {
        let evt = Date.now().toString() + 'jv';
        this.CorekProvider.socket.emit('get_users', {
          'event': evt,
          'key':'validemail',
          'condition':{
            'user_email': this.email}
        }); 
        this.CorekProvider.socket.on(evt, (r , k)=>{
          console.log(r);
          if(r.length > 0){
            loader.dismiss();
            let alert = this.alertCtrl.create({
              title: 'Espere',
              subTitle: 'El email ya se encuentra en uso.',
              buttons: ['OK']
            });
            alert.present();
          }else{
                let nf2 = Date.now().toString()+'inseruser';  
              let registrando = {'insert' : {
                'user_email': this.email,
                'user_pass':  this.password,
                'user_login':this.nombre,
                },
                'event':nf2
              }
              this.CorekProvider.socket.emit('insert_user',registrando);
              this.CorekProvider.socket.on(nf2, (data,key)=>{
                console.log(data);
                if(data.insertId != 0 )
                {
                  console.log(data.insertId);
    
                  let meta = { 'insert': {  
                      'user_id': data.insertId, //id
                      'meta_key':'wp_user_level',
                      'meta_value': 0
                    },
                      'key':'level'
                  }
                  this.CorekProvider.socket.emit('add_user_meta' , meta);
                  let meta1;
                  if(this.tipo == '1')
                  {
                    meta1 = { 'insert': {
                      'user_id': data.insertId, //id
                      'meta_key':'wp_capabilities',
                      'meta_value': 'a:1:{s:7:"usuario";b:1;}'
                      },
                      'key':'capabilities'
                    }
                  } 
                  else if(this.tipo == '2')
                  {
                    meta1 = { 'insert': {
                      'user_id': data.insertId, //id
                      'meta_key':'wp_capabilities',
                      'meta_value': 'a:1:{s:8:"adminapp";b:1;}'
                    },
                        'key':'capabilities'
                    }
                  } 
                  else if(this.tipo == '3')
                  {
                    meta1 = { 'insert': {
                      'user_id': data.insertId, //id
                      'meta_key':'wp_capabilities',
                      'meta_value': 'a:1:{s:7:"policia";b:1;}'
                    },
                        'key':'capabilities'
                    }
                  }
                                
                  this.CorekProvider.socket.emit('add_user_meta' , meta1);
                  let meta2 = { 'insert': {
                      'user_id': data.insertId, //id
                      'meta_key':'first_name',
                      'meta_value': this.nombre
                    },
                        'key':'capabilities'
                  }
                  this.CorekProvider.socket.emit('add_user_meta' , meta2);
                  meta2 = { 'insert': {
                    'user_id': data.insertId, //id
                    'meta_key':'residencia',
                    'meta_value': this.residencia
                  },
                      'key':'res'
                }
                this.CorekProvider.socket.emit('add_user_meta' , meta2);
                meta2 = { 'insert': {
                  'user_id': data.insertId, //id
                  'meta_key':'_residencia',
                  'meta_value': 'field_5ae757c42adf8'
                },
                    'key':'res'
              }
              this.CorekProvider.socket.emit('add_user_meta' , meta2);
                
                  meta2 = { 'insert': {
                    'user_id': data.insertId, //id
                    'meta_key':'telefono',
                    'meta_value': this.telefono
                  },
                      'key':'tel'
                }
                this.CorekProvider.socket.emit('add_user_meta' , meta2);
                if(this.tipo === '3'){
                  this.CorekProvider.socket.emit('add_user_meta' , meta2);
                  meta2 = { 'insert': {
                    'user_id': data.insertId, //id
                    'meta_key':'placa',
                    'meta_value': this.idplaca
                  },
                      'key':'tel'
                }
                this.CorekProvider.socket.emit('add_user_meta' , meta2);
                }
                  
                  let meta4 = { 'insert': {
                      'user_id': data.insertId, //id
                      'meta_key':'nickname',
                      'meta_value': this.email
                    },
                        'key':'ultimo'
                  }
                  this.CorekProvider.socket.emit('add_user_meta' , meta4);
                  this.CorekProvider.socket.on('add_user_meta', (data, key)=>{
                    if(key == 'ultimo'){
                      let alert = this.alertCtrl.create({
                        title: 'Registro Exitoso',
                        subTitle: 'El administrador debe activar su cuenta.',
                        buttons: ['OK']
                      });
                      loader.dismiss();
                      alert.present();
                      this.navCtrl.pop();
                    }
                  });
                }
              });
          }
        });
      });
    });

  }    
}
