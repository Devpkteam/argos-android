import { Component , ViewChild, ElementRef } from '@angular/core';
import {  NavController, NavParams , IonicPage } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
// import {  ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { CorekProvider } from '../../providers/corek/corek';
// import { DenunciaPage } from '../denuncia/denuncia';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { DenunciaPage } from '../denuncia/denuncia';
// import { Geolocation, Geoposition } from '@ionic-native/geolocation';

/**
 * Generated class for the LocationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var google;
@IonicPage()
@Component({
  selector: 'page-location',
  templateUrl: 'location.html',
})
export class LocationPage {
  autocompleteItems: any;
  geocoder;
  autocomplete =  {query: ''};
  acService:any;
  placesService: any;
  @ViewChild('map') mapElement: ElementRef;
  map: any;
  address = '';
  addressrequest;
  seta: any;
  lat;
  lng;
  name;
  id;
  res;
  idDenuncia;
  constructor(private storage: Storage, public CorekProvider: CorekProvider, private geolocation : Geolocation, public navCtrl: NavController, public navParams: NavParams , private locationAccuracy: LocationAccuracy) {
    this.storage.get('nickname').then((data)=>{
      this.name = data;
      });
      this.storage.get('residencia').then((data)=>{
        this.res = data;
      });
      this.storage.get('alertDenuncia').then((data)=>{
        this.idDenuncia = data;
      });
    this.geolocation.getCurrentPosition().then((resp) => {
      // resp.coords.latitude
      // resp.coords.longitude
     console.log(resp);
      this.initMap(resp.coords.latitude, resp.coords.longitude); 
      
     }).catch((error) => {
       console.log('Error getting location', error);
     
     });
     
     let watch = this.geolocation.watchPosition();
     watch.subscribe((data) => {
      // data can be a set of coordinates, or an error (if an error occurred).
      // data.coords.latitude
      // data.coords.longitude
     });
  }

  ionViewWillEnter(){
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {

      if(canRequest) {
        // the accuracy option will be ignored by iOS
        this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
          () => console.log('Request successful'),
          error => console.log('Error requesting location permissions', error)
        );
      }
    
    });
  }
  initMap(lat, lng)
  {
    var uluru = {lat: this.lat, lng: this.lng};
    this.lng = lng;
    this.lat = lat;
    console.log('Error aqui');
    let latLng = new google.maps.LatLng(lat, lng);
    let mapOptions = {
     center: latLng,
     zoom: 18,
     mapTypeId: google.maps.MapTypeId.ROADMAP,
     disableDoubleClickZoom: true,
     fullscreenControl: true,
     panControl: true,
     scaleControl: true,
     draggable: true, zoomControl: true, scrollwheel: true
    }
    
   this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
   var marker = new google.maps.Marker({
    map: this.map,
    position: latLng,
    title: "Estas aqui"
  });
    
  }

  sendLocation()
  {
    let xs = new Date().toISOString() + 'l';
    this.CorekProvider.ConnectCorekconfig(xs);
    this.CorekProvider.socket.on(xs , (data)=>{
      console.log(data);
      this.storage.get('residencia').then((val) => {
        console.log(val);
        this.id = val;
        console.log(this.lat, this.lng)
        // let url = "https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d980.7704567567267!2d-"+this.lng+"!3d"+this.lat+"!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses-419!2sve!4v1527628285041"
        // let url = 'https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d15692.2304959306!2d'+this.lng+'!3d'+this.lat+'!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses-419!2sve!4v1529031329517&zoom=20';
        // let url = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3923.398910730274!2d-'+this.lat+'!3d'+this.lng+'!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMTDCsDI4JzA5LjEiTiA2NsKwNTAnMzMuNSJX!5e0!3m2!1ses!2sve!4v1533982563810';
        let url = 'https://maps.google.com/maps?width=100%&height=600&hl=es&coord='+this.lat+','+this.lng+'&q='+this.lat+','+this.lng+'&ie=UTF8&t=&z=14&iwloc=B&output=embed';
        // let html = '' 
        console.log(url);
        let nf = Date.now().toString()+'confipri';
        let condition ={
          'post_mime_type':'map',
          'post_content':this.lat,
          'post_name':this.lng,
          'post_excerpt': this.idDenuncia ,
          'post_content_filtered': url,
          'post_type':'comments',
          'post_title': this.name,
          'post_author':this.id,
          'post_date': new Date(),
          "to_ping": 1
        }
        console.log("enviando direccion ...");
        console.log(condition);
        this.CorekProvider.socket.emit('insert_post',{'condition': condition,'key':'insertpost','event':nf});
        this.CorekProvider.socket.on(nf, (data)=>{
          console.log(data);
        });
        this.CorekProvider.socket.on(nf , (data , key )=>{
          console.log('inserto ' , data , key);
          let nf = Date.now().toString()+"query";
          this.CorekProvider.socket.emit('query_post', {"condition":{'ID':data.insertId},'event':nf});
          this.CorekProvider.socket.on(nf , (data)=>{
            this.storage.get('nameRes').then((nameRes) => {
              this.CorekProvider.socket.emit('newpost',{'condition': data[0] , event:'msjto' + this.res});
              let notificacion = {
                android:{
                  priority: 'normal',
                  notificacion:{
                    icon: 'http://argos.corek.io/chat.png',
                    color: '#ebc041',
                    title:nameRes, 
                    body:data[0].post_content,
                    sound:'default',
                  },
                  'topic':this.res,
                }
                
              }
              this.CorekProvider.socket.emit('emitnotification', notificacion);
            });
          })
      });
        this.navCtrl.setRoot(DenunciaPage);
        this.navCtrl.popToRoot();
      });
    });
    
  }

  geocodeAddress(geocoder, resultsMap,callback) {
    console.log(resultsMap);
    let mapa;
    geocoder.geocode({'address': this.address}, function(results, status) {
      if (status === 'OK') {
        resultsMap.setCenter(results[0].geometry.location);
        mapa = results[0];
        console.log(results[0]);
        this.seta = results[0].place_id;
        console.log(this.seta);
        // var marker = new google.maps.Marker({
        //   map: resultsMap,
        //   position: results[0].geometry.location
        // });
       callback(mapa);
      } else {
        console.log(this.address);
        if(this.address != '' || this.address != undefined){
          // alert('Geocode was not successful for the following reason: ' + status);
        }else{
          
        }
      }
    });
    return resultsMap; 
}

getadress()
{
  console.log(this.address);
  this.geocodeAddress(this.geocoder, this.map,(val)=> {
      console.log(val);
    });
}


  ionViewDidLoad() {
    console.log('ionViewDidLoad LocationPage');
  }
  
  close(){
    this.navCtrl.setRoot(DenunciaPage);
        this.navCtrl.popToRoot();
  }
}
