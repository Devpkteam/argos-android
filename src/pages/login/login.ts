import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Platform } from 'ionic-angular';
import { CorekProvider } from '../../providers/corek/corek';
import { RegistroPage } from '../registro/registro';
import { AlertController } from 'ionic-angular';
// import { DashboardPage } from '../dashboard/dashboard';
import { Storage } from '@ionic/storage';
import { LoadingController } from 'ionic-angular';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Geolocation } from '@ionic-native/geolocation';
import { Media, MediaObject } from '@ionic-native/media';
import { Camera, CameraOptions } from '@ionic-native/camera';

// import { DenunciaPage } from '../denuncia/denuncia';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  tipo;
  email;
  password;
  recordar = true;
  res;
  nombre;
  id;
  sesion;
  fileName;
  file;
  audio;
  filePath;
  constructor(private camera: Camera,public platform: Platform,private media: Media ,private geolocation : Geolocation,private locationAccuracy: LocationAccuracy,public loadingCtrl: LoadingController, private storage: Storage, public CorekProvider: CorekProvider, public navCtrl: NavController, public navParams: NavParams , public alertCtrl: AlertController) {
    this.tipo = this.navParams.get('rol');
  }
       nextperm(){
                 this.locationAccuracy.canRequest().then((canRequest: boolean) => {

      if(canRequest) {
        // the accuracy option will be ignored by iOS
        this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
          () => this.nextpermito(),
          error => this.nextpermito()
        );
      }
    
    });
       } 
nextpermito(){
          if (this.platform.is('ios')) {
            this.fileName = 'record'+new Date().getDate()+new Date().getMonth()+new Date().getFullYear()+new Date().getHours()+new Date().getMinutes()+new Date().getSeconds()+'.3gp';
            this.file.createFile(this.file.tempDirectory, 'my_file.m4a', true).then(() => {
            this.audio = this.media.create(this.file.tempDirectory.replace(/^file:\/\//, '') + 'my_file.m4a');
            this.audio.startRecord();
          });
          } else if (this.platform.is('android')) {
            this.fileName = 'record'+new Date().getDate()+new Date().getMonth()+new Date().getFullYear()+new Date().getHours()+new Date().getMinutes()+new Date().getSeconds()+'.3gp';
            this.filePath = this.file.externalDataDirectory.replace(/file:\/\//g, '') + this.fileName;
            this.audio = this.media.create(this.filePath);
          }

    this.audio.startRecord();
        this.audio.stopRecord();
}

  login()
  { 
    let loader = this.loadingCtrl.create({
      content: "Espere",
      duration:10000
    });
    loader.present();
    console.log("logueando");// aqui se verifica la conexion del proyecto con corek
    let nf = Date.now().toString()+'conectlogin';
    this.CorekProvider.ConnectCorekconfig(nf); 
    this.CorekProvider.socket.on(nf, (data, key) => {
      console.log(data);
      if(data.conf ==  true){
        let datarequest= {
          'email':this.email,
          'password':this.password
        }
        let event = Date.now().toString()+'finishlogin';
        this.CorekProvider.login(datarequest,event);
        console.log('emit login ');
        this.CorekProvider.socket.on(event, (data , key)=>{
          console.log(data);
          console.log(key);
          if(data.user_status == 0){
              console.log("cuneta inactiva");
              loader.dismiss();
              let alert = this.alertCtrl.create({
                title: 'Lo sentimos',
                subTitle: 'Su cuenta  aun no ha sido activada.',
                buttons: ['OK']
              });
              alert.present();
          }else{
            if(data.token)
            {
              this.sesion = data.token;
              this.nombre = key.datauser.user_login;
              this.id = data.ID;
              console.log(this.nombre);
              let nf = Date.now().toString()+"va";
              this.CorekProvider.socket.emit('get_user_meta',{"condition":{"user_id":this.id, "meta_key":"residencia"}, 'event':nf});
              this.CorekProvider.socket.on(nf, (data2,key)=>{
                if(this.recordar == true)
                {
                  this.res = data2[0].meta_value;
                  console.log(data2);
                  console.log("mi id residencia "+this.res);
                  this.storage.set('remember', true);  
                  this.storage.set('session', this.sesion);     
                  this.storage.set('nickname', this.nombre);            
                  this.storage.set('ID', this.id);
                  this.storage.set('residencia', this.res);
                  this.storage.set('alertDenuncia', -1);
                  loader.dismiss();
                  let evt = Date.now().toString() + 'jv';
                  this.CorekProvider.socket.emit('get_users', {'event': evt,'condition':{'ID': this.id}}); 
                  this.CorekProvider.socket.on(evt, (r , k)=>{
                    console.log(r[0].user_status);
                    this.storage.set('rol', r[0].user_status);  ;
                  }); 
                   this.navCtrl.pop();



                   
                }
                else
                {
                  this.res = data2[0].meta_value;
                  console.log("mi id residencia "+this.res);
                  console.log("no recordar usuario");
                  this.storage.set('rol', this.tipo);  
                  this.storage.set('remember', false);  
                  this.storage.set('session', data.token);     
                  this.storage.set('nickname', this.nombre);            
                  this.storage.set('ID', this.id);
                  this.storage.set('residencia', this.res);
                  this.storage.set('alertDenuncia', -1);
                  loader.dismiss();  
                  this.navCtrl.pop();
                }
              });
            }
            else
            {
              loader.dismiss();  
              let alert = this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Verifique el correo o la contraseña',
                buttons: ['OK']
              });
              alert.present();
            }
          }
        });
      }
    });
  }



    ionViewDidLoad() {
      console.log('ionViewDidLoad LoginPage');
    }
    
    registrar(){
      this.navCtrl.push(RegistroPage, {
        rol: this.tipo
      });
    }

    recovery()
    {
      let prompt = this.alertCtrl.create({
        title: 'Recuperar Contraseña',
        message: "Ingrese su email, le enviaremos un correo con el enlace para cambiar su contraseña.",
        inputs: [
          {
            name: 'email',
            placeholder: 'Email',
            type: 'email',
          },
        ],
        buttons: [
          {
            text: 'Cancel',
            handler: data => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'Recuperar',
            handler: data => {          
              console.log(data.email);
              if(data.email == '' || data.email == undefined)
              {
                console.log("campo vacio");
              }
              else
              {              
                console.log('recuperando ..');
                let nf = Date.now().toString()+'confipri';
                this.CorekProvider.ConnectCorekconfig(nf); 
                this.CorekProvider.socket.on(nf, (data2, key) =>{
                  let loader = this.loadingCtrl.create({
                    content: "Espere"
                  });
                  loader.present();
                  setTimeout(() => {
                    loader.dismiss();
                  }, );
                  let event = Date.now().toString()+'recover';
                  let datarequest= {
                    'email':data.email,
                    'token':1
                  }
                  this.CorekProvider.recover_password(datarequest,event);
                });
              }
            }
          }
        ]
      });
      prompt.present();
    }
}