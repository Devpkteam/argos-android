import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { Storage } from '@ionic/storage';
import { DenunciaPage } from '../denuncia/denuncia';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController, private storage: Storage) {
  
  }
  goLogin(tipo){
    console.log(tipo);
    this.navCtrl.push(LoginPage , {rol:tipo});
    // this.navCtrl.pop();
  }
  ionViewWillEnter(){

    this.storage.get('session').then((r) => {
      if(r !== null){
        // splashScreen.hide();
        // this.rootPage =DenunciaPage;
        // this.navCtrl.pop();
        this.navCtrl.setRoot(DenunciaPage);
        this.navCtrl.popToRoot();
      }
    });
  }
}
