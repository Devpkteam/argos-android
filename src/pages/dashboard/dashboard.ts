import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , ViewController , Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { AlertController } from 'ionic-angular';
// import { HomePage } from '../home/home';
// import { LoginPage } from '../login/login';
import { DenunciaPage } from '../denuncia/denuncia';
import { CorekProvider } from '../../providers/corek/corek';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { LoadingController } from 'ionic-angular';
import { SMS } from '@ionic-native/sms';
import { ToastController } from 'ionic-angular';
import { AndroidPermissions } from '@ionic-native/android-permissions';
// import { Push, PushObject, PushOptions } from '@ionic-native/push';
 import { FCM } from '@ionic-native/fcm';
 import { PermisosPage } from '../permisos/permisos';
import { HomePage } from '../home/home';
/**
 * Generated class for the DashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {
  iduser;
  tipo;
  name;
  currentuser;
  res;
  arraypush;
  idDenuncia;
  rol;
  loader = this.loadingCtrl.create({
    content: "Espere"
  });
//   options: PushOptions = {
//     android: {},
//     ios: {
//         alert: 'true',
//         badge: true,
//         sound: 'false'
//     },
//     windows: {},
//     browser: {
//         // pushServiceURL: 'http://push.api.phonegap.com/v1/push'
//     }
//  };
  // pushObject: PushObject = this.push.init(this.options);
  constructor(private fcm: FCM,private toastCtrl: ToastController, private sms: SMS, public loadingCtrl: LoadingController, public CorekProvider: CorekProvider, public storage : Storage, public alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams , public viewCtrl : ViewController , private locationAccuracy: LocationAccuracy , private androidPermissions: AndroidPermissions , public platform: Platform ) {
    // this.tipo = this.navParams.get('rol');
     this.storage.get('ID').then((val) => {
                    this.iduser = val;
                //  empieza notificaciones
                  fcm.subscribeToTopic('all');
                  let nf = Date.now().toString()+"conectloginconf";
                  console.log('emit');
                  this.CorekProvider.ConnectCorekconfig(nf); 
                  this.CorekProvider.socket.on(nf ,(dat)=>{
                  let nf = 'confpri1';
                  this.CorekProvider.socket.on('connection', (data)=>{
                  this.CorekProvider.socket.emit('conf', { 'project': 'http://argos.com','event':nf});
                  this.CorekProvider.socket.on(nf, (data) => {
                    fcm.getToken().then(token=>{
                        console.log(token);
                        let cnow = Date.now().toString()+'newnotification';
                        this.CorekProvider.socket.emit('newnotification',{'id_user':this.iduser,'token':token,'event':cnow});
                      
                        this.CorekProvider.socket.on(cnow, (data)=>{
                          cnow = Date.now().toString()+'emitnotification';
                          this.CorekProvider.socket.emit('emitnotification',{'id_user':this.iduser,'event':cnow,'title':'Bienvenido','body':'Muchas gracias por ingresar nuevamente'});
                        });

                    });

                    fcm.onNotification().subscribe(
                      data=>{
                
                      if(data.wasTapped){
                                          
                        console.log("Received in background");
                      } else {
                                
                        console.log("Received in foreground");
                      };
                    },
                  msg=>{
                            let confirm = this.alertCtrl.create({
                                                          title: msg.title,
                                                          message: msg.body,
                                                          buttons: [
                                                            // {
                                                            //   text: 'Disagree',
                                                            //   handler: () => {
                                                            //     console.log('Disagree clicked');
                                                            //   }
                                                            // },
                                                            {
                                                              text: 'Ok',
                                                              handler: () => {
                                
                                                              }
                                                            }
                                                          ]
                                                        });
                                                        confirm.present();
                  }
                  )

                    fcm.onTokenRefresh().subscribe(token=>{
                      let cnow = Date.now().toString()+'newnotification';
                                this.CorekProvider.socket.emit('newnotification',{'id_user':this.iduser,'token':token,'event':cnow});
                        //           this.CorekProvider.socket.on(cnow, (data)=>{
                        // cnow = Date.now().toString()+'emitnotification';
                        //       this.CorekProvider.socket.emit('emitnotification',{'id_user':this.iduser,'event':cnow});
                        //           });
                                    //  let confirm = this.alertCtrl.create({
                                    //                       title: 'token',
                                    //                       message: token,
                                    //                       buttons: [
                                    //                         // {
                                    //                         //   text: 'Disagree',
                                    //                         //   handler: () => {
                                    //                         //     console.log('Disagree clicked');
                                    //                         //   }
                                    //                         // },
                                    //                         {
                                    //                           text: 'Ok',
                                    //                           handler: () => {
                                
                                    //                           }
                                    //                         }
                                    //                       ]
                                    //                     });
                                    //                      confirm.present();
                    })

                    // fcm.unsubscribeFromTopic('marketing');
                              });
                  });
                })
                  //termina notificaciones

     });
    // this.push.hasPermission()
    // .then((res: any) => {

    //   if (res.isEnabled) {
    //     console.log('We have permission to send push notifications');
    //   } else {
    //     console.log('We do not have permission to send push notifications');
    //   }

    // });
    
    // this.pushObject.on('registration').subscribe((registration: any) => console.log('Device registered', registration));

    this.storage.get('nickname').then((data)=>{
      this.name = data;
    });
    if (this.platform.is('ios')) {

    } if (this.platform.is('android')) {
      this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.CAMERA).then(
        result => console.log('Has permission?',result.hasPermission),
        err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.CAMERA)
      );
      this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.MEDIA_CONTENT_CONTROL).then(
        result => console.log('Has permission?',result.hasPermission),
        err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.MEDIA_CONTENT_CONTROL)
      );
      this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(
        result => console.log('Has permission?',result.hasPermission),
        err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE)
      );
      this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_FRAME_BUFFER).then(
        result => console.log('Has permission?',result.hasPermission),
        err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_FRAME_BUFFER)
      );
      this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.RECORD_AUDIO).then(
        result => console.log('Has permission?',result.hasPermission),
        err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.RECORD_AUDIO)
      );
      this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.SEND_SMS).then(
        result => console.log('Has permission?',result.hasPermission),
        err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.SEND_SMS)
      );
      this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE).then(
        result => console.log('Has permission?',result.hasPermission),
        err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE)
      );
    }
    this.storage.get('rol').then((data)=>{
      this.tipo = data;
      console.log(this.tipo);
    });
    
    this.storage.get('residencia').then((data)=>{
      this.res = data;
      fcm.subscribeToTopic(this.res);
    }); 
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {

      if(canRequest) {
        // the accuracy option will be ignored by iOS
        this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
          () => {
            console.log("solic")
          },
          error => {console.log("err")}
        );
      }
    
    });
    console.log("willenter");
    this.storage.get("settings").then((d)=>{
      console.log(d);
      if(d == null){
        this.navCtrl.push(PermisosPage);
      }
    
    });
    if(this.currentuser == ''){
      this.storage.get('ID').then((data)=>{
        this.currentuser = data;
        console.log(this.currentuser);
        let nf1 = Date.now().toString()+'conectlogin';
        this.CorekProvider.ConnectCorekconfig(nf1); 
        this.CorekProvider.socket.on(nf1, (data, key) => {
          let evt = Date.now().toString() + 'jv';
          this.CorekProvider.socket.emit('get_users', {'event': evt,'condition':{'ID': this.currentuser}}); 
          this.CorekProvider.socket.on(evt, (r , k)=>{
            this.rol = r[0].user_status;
            console.log(this.rol);
          });
        });
      });
    }else{
      console.log('ya tengo id')
      this.storage.get('alertDenuncia').then((d)=>{
        console.log(d);
        if(d != -1){
          // this.navCtrl.popToRoot();
        }
      });
    }
  }



  ionViewDidLoad() {
    console.log('ionViewDidLoad DashboardPage');
    this.viewCtrl.showBackButton(false);
  }
  activateSts()
  {
    this.storage.get('rol').then((data)=>{
      this.tipo = data;
      console.log(this.tipo);
      if(this.tipo == 1){
        let  confirm = this.alertCtrl.create({
          title: 'Espere',
          message: 'Solo administradores y policias pueden ejecutar esta funcion.',
          buttons: [
            {
              text: 'Aceptar',
              handler: () => {
                console.log(this.currentuser);
                // this.sms.send('04142535107', 'SN5539T9');
              }
            }
          ]
        });
        confirm.present();
      }
      else
      {
        let  confirm = this.alertCtrl.create({
          title: '¿Estas Seguro?',
          message: 'Pulse aceptar para activar el sts.',
          buttons: [
            {
              text: 'Cancelar',
              handler: () => {
                console.log('Disagree clicked');
              }
            },
            {
              text: 'Aceptar',
              handler: () => {
                this.storage.get('ID').then((data)=>{
                  this.currentuser = data;
                
                  console.log(this.currentuser);
                  let nf = Date.now().toString()+'confipri';
                  this.CorekProvider.ConnectCorekconfig(nf); 
                  this.CorekProvider.socket.on(nf, (data, key) =>{
                    let nf1 = Date.now().toString()+"ins";
                    let condition ={
                      'post_author':this.res,
                      'post_title': this.name,
                      'post_parent': this.currentuser,
                      'post_date': new Date(),
                      'post_type': 'alertSts',
                      'guid': this.tipo         
                    }
                    console.log(condition);
                    this.CorekProvider.socket.emit('insert_post',{'condition': condition,'key':'insertpost','event':nf1});
                  });
                  this.sms.send('04142535107', 'SN5539T9');
                });
              }
            }
          ]
        });
        confirm.present();
      }
    });
  }
  exit()
  {
    console.log("salir");
    let  confirm = this.alertCtrl.create({
      title: '¿Desea Salir?',
      message: 'Esta cerrando sesión, realmente desea salir.',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Salir',
          handler: () => {
            console.log('Agree clicked');
            this.storage.remove("remember");
            this.storage.remove("session");
            this.storage.remove("nickname");
            this.storage.remove("ID");
            this.storage.remove("residencia");
            this.storage.remove("alertDenuncia");
            this.storage.remove("rol");
            // this.storage.clear();
            this.navCtrl.setRoot(HomePage);
            this.navCtrl.popToRoot();
          }
        }
      ]
    });
    confirm.present();
  }
  ionViewWillEnter(){

    this.storage.get('session').then((r) => {
      if(r == null || r == ""){
        // splashScreen.hide();
        // this.rootPage =DenunciaPage;
        // this.navCtrl.push(HomePage);
        this.navCtrl.setRoot(HomePage);
        this.navCtrl.popToRoot();
      }
    });
  }

  denunciar()
  {
    this.loader = this.loadingCtrl.create({
      content: "Espere",
      duration: 10000
    });
    this.loader.present();
    
    let nf = Date.now().toString()+"231";
    console.log('emit');
  
    // let nf = Date.now().toString()+'confipri';
    this.CorekProvider.ConnectCorekconfig(nf); 
    this.CorekProvider.socket.on(nf, (data, key) =>{
      console.log('on de conection 380');
       this.storage.get('residencia').then((data)=>{
      this.res = data;
      console.log('Residencia ID' + this.res);
      let nn = Date.now().toString()+"query2";
      this.CorekProvider.socket.emit('query_post', { 'condition': { 'post_author':this.res , 'post_type': 'alertDenuncia' , 'post_status':'publish' }, 'event': nn } );
                  this.CorekProvider.socket.on(nn , (res)=>{
                      console.log(res);
                      if(res.length > 0){
                        console.log('on');
                        this.storage.set('alertDenuncia' , res[0].ID);
                        this.loader.dismiss();
                         this.storage.get('alertDenuncia').then((data)=>{
                         this.navCtrl.setRoot(DenunciaPage);
                         this.navCtrl.popToRoot();
                         });
                      }else{
                        this.loader.dismiss();
                        this.createDenuncia();
                      }
                  });
       });
    });
  }
  createDenuncia(){
    let  confirm = this.alertCtrl.create({
      title: '¿Estas Seguro?',
      message: 'Pulse aceptar para generar una denuncia.',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('Disagree clicked');
            // this.loader.dismiss();
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            // this.loader.dismiss();
            this.storage.get('rol').then((data)=>{
              this.tipo = data;
              let nf1 = Date.now().toString()+"confipris";
              this.CorekProvider.ConnectCorekconfig(nf1);
              this.CorekProvider.socket.on(nf1, (data) => {
                let nf = Date.now().toString()+"querypost";
                let nf1 = Date.now().toString()+"ins";
                let condition ={
                  'post_author':this.res,
                  'post_title': this.name,
                  'post_parent': this.currentuser,
                  'post_date': new Date(),
                  'post_type': 'alertDenuncia',
                  'guid': this.tipo          
                }
                console.log(condition);
                this.CorekProvider.socket.emit('insert_post',{'condition': condition,'key':'insertpost','event':nf1});
                this.CorekProvider.socket.on(nf1 , (d)=>{
                  console.log(d);
                  this.CorekProvider.socket.emit('newpost',{'condition': {'post_author':this.res , 'open':d.insertId} , event:'open' + this.res , 'key':'archived'});
                  this.storage.set('alertDenuncia', d.insertId);
                   this.storage.get('alertDenuncia').then((data)=>{
                  let not = {
                    notification: {
                      title: condition.post_title, 
                      body:"ha realizado una denuncia.",
                    },
                    android: {
                      notification: {
                        // icon: 'http://argos.corek.io/chat.png',
                        color: '#ebc041',
                        sound:'default',
                      },
                    },
                    'topic':this.res,
                   };
                  this.CorekProvider.socket.emit('emitnotification', {notification:not});
                  
                  this.navCtrl.setRoot(DenunciaPage);
                        this.navCtrl.popToRoot();
                   });
                });
              });
            });
          }
        }
      ]
    });
    confirm.present();
  }
  Denuncia()
  {
    let loader = this.loadingCtrl.create({
      content: "Espere"
    });
    loader.present();
    this.storage.get('alertDenuncia').then((data)=>{
      this.idDenuncia = data;
      let nf1 = Date.now().toString()+"confipris";
      this.CorekProvider.ConnectCorekconfig(nf1);
      this.CorekProvider.socket.on(nf1, (data) => {
        let nf = Date.now().toString()+"querypost";
        this.CorekProvider.socket.emit('query_post', { 'condition': { 'post_type': 'alertDenuncia', 'post_author':this.res, 'post_status':'publish'}, 'event': nf });
        this.CorekProvider.socket.on(nf, (data, key) => {
          console.log(data);
          
          if(data.length > 0 && this.idDenuncia)
          {
            console.log("hay algo");
            loader.dismiss();
            this.navCtrl.push(DenunciaPage);  
          }
          else
          {
            let alert = this.alertCtrl.create({
              title: 'Aun no hay denuncia',
              subTitle: 'Debe existir una denuncia para activar el chat',
              buttons: ['OK']
            });
            loader.dismiss();
            alert.present();
          }
        });
      });
    });
  }
}