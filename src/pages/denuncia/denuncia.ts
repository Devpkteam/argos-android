import { Component,  ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams , Platform } from 'ionic-angular';
import { CorekProvider } from '../../providers/corek/corek';
import { LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ActionSheetController } from 'ionic-angular';
import { Content } from 'ionic-angular';
import { Media, MediaObject } from '@ionic-native/media';
import { File } from '@ionic-native/file';
import { SMS } from '@ionic-native/sms';
import { ToastController } from 'ionic-angular';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
// import { VideoPlayer } from '@ionic-native/video-player';
import { Device } from '@ionic-native/device';
import { HomePage } from '../home/home';
import { LocationPage } from '../location/location';
import { AlertController } from 'ionic-angular';
import {DomSanitizer} from '@angular/platform-browser'
import { DashboardPage } from '../dashboard/dashboard';
import { FCM } from '@ionic-native/fcm';
import { PhotoViewer } from '@ionic-native/photo-viewer';

/**
 * Generated class for the DenunciaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-denuncia',
  templateUrl: 'denuncia.html'
})
export class DenunciaPage {
  mensaje = '';
  mensaje1 = '';
  // fileName;
  arraypush;
  vari;
  vari2;
  isStartRecording;
  res;
  id;
  name;
  image;
  isNewRecord;
  isPlayingRecord;
  nameRes;
  rol;
  idDenuncia;
  tipo;
  // IdChat = 936;
  currentuser;
  rec = false;
  playing;
  recording: boolean = false;
  filePath: string;
  fileName: string;
  audio: MediaObject;
  audioList: any[] = [];  
  @ViewChild(Content) content: Content;
  public mediaObject: MediaObject ;
  os;
  fileNameIos;
  constructor(private photoViewer: PhotoViewer,private alertCtrl: AlertController, private fcm: FCM ,  private storage: Storage, public loadingCtrl: LoadingController, public CorekProvider: CorekProvider, public navCtrl: NavController, public navParams: NavParams, private camera: Camera, public actionSheetCtrl: ActionSheetController, private media: Media ,  private file: File , private sms: SMS , public toastCtrl: ToastController , private transfer: FileTransfer ,   private device: Device , public platform: Platform , private sanitizer: DomSanitizer) {
   


      
      
  }
  ionViewWillEnter(){

    this.storage.get('session').then((r) => {
      if(r == null || r == ""){
        this.navCtrl.setRoot(HomePage);
        this.navCtrl.popToRoot();
        
      }
      else{
      
     
    
    console.log(this.device.platform );
      
    this.storage.get('nickname').then((data)=>{
      this.name = data;
    });
    this.storage.get('ID').then((data)=>{
      this.id = data;
    });

       this.storage.get('residencia').then((data)=>{
        if(data == null || data == ""){
          // splashScreen.hide();
          // this.rootPage =DenunciaPage;
          this.navCtrl.push(DashboardPage);
        }
        this.res = data;
          let nn = Date.now().toString()+"query";
                     let nfq = Date.now().toString()+'confipri';
                            this.CorekProvider.ConnectCorekconfig(nfq); 
                            this.CorekProvider.socket.on(nfq, (data, key) =>{
              this.CorekProvider.socket.emit('query_post', { 'condition': { 'post_author':this.res , 'post_type': 'alertDenuncia' , 'post_status':'publish' }, 'event': nn } );
                          this.CorekProvider.socket.on(nn , (res)=>{
                            
                            if(res.length ==0){
                              this.navCtrl.push(DashboardPage);
                            }
                                      this.idDenuncia = res[0].ID;
                                      console.log(this.idDenuncia);
                                  
                              
                                      this.storage.get('rol').then((data)=>{
                                        this.rol = data;
                                        console.log(this.rol);
                                          this.storage.get('ID').then((data)=>{
                                            this.id = data;
                                            console.log(this.id);
                                            console.log(this.id);
                                            console.log(this.res);
                                            let nf = Date.now().toString()+"ins";
                                            this.CorekProvider.socket.emit('query_post', { 'condition': {'post_author':this.res,"post_status":"publish", "post_type":"comments" , "post_excerpt":this.idDenuncia} , 'key': 'notificacion', 'event': nf });
                                            this.CorekProvider.socket.on(nf, (data, key) => {
                                              // console.log(data);
                                              this.arraypush = data;
                                              for (let d of data) {
                                                // this.arraypush.push(d);
                                                if(d.post_mime_type == 'map'){
                                                  console.log('Mapa');
                                                  console.log(d);
                                                  let sf = this.sanitizer.bypassSecurityTrustResourceUrl(d.post_content_filtered);
                                                  d.urlsafe = sf;
                                                }
                                              }
                                              // this.CorekProvider.socket.on('msjto' + this.res,(data, key) => {
                                              //   console.log(data);
                                              //   if(this.res==data.condition.post_author)
                                              //   {
                                              //     console.log('bajar'); 
                                              //     this.arraypush.push(data.condition);
                                              //     setTimeout(() => {
                                              //       this.scrollToBottom();
                                              //     }, 200);
                                              //   }
                                              // });
                                            });
                                          
                                            console.log(this.res);
                                            let nf2 = Date.now().toString()+"ins";
                                            this.CorekProvider.socket.emit('query_post', { 'condition': {"ID":this.res} ,'event': nf2 });
                                            this.CorekProvider.socket.on(nf2, (data3, key) => {
                                              console.log(data3[0].post_title);
                                              this.nameRes = data3[0].post_title;
                                              this.storage.set('nameRes', this.nameRes);  
                                            });
                                            setTimeout(() => {
                                              this.scrollToBottom();
                                            }, 3000);
                                          });
                                      });
                                
                              });
                // fcm.subscribeToTopic(this.res);
                this.CorekProvider.socket.on('msjto' + this.res ,  (data , key ) => {
                                    console.log(data);
                                    if(this.res == data.condition.post_author)
                                    {
                                      if(data.condition.action === 'close'){
                                        console.log('Cerrar'); 
                                        
                                      }else{
                                        console.log('bajar'); 
                                      console.log(data);
                                      let nr= this.arraypush.length;
                                      let nr2=nr-1;
                                      if(nr2>0){
                                        nr2=0;
                                      }
                                      console.log(this.arraypush[nr2]);
                                      console.log(data.condition.post_date);
                                      let pass=false;
                                      if(this.arraypush[nr2]!==undefined && typeof(this.arraypush[nr2]) && this.arraypush[nr2].post_date !== data.condition.post_date){
                                          pass= true;
                                      }
                                        else if(this.arraypush[nr2]==undefined){
                                          pass=true;
                                        }
                                      if(pass){

                                      
                                        data.condition.ID=nr;
                                      if(data.condition.post_mime_type=='audio'){
                                          let link=data.condition.post_content;
                                          let name =data.condition.post_name;
                                          let idx = nr;
                                          console.log(link);
                                          const  url = 'http://' +link;
                                          console.log(name);
                                          console.log(idx);
                                          const fileTransfer: FileTransferObject = this.transfer.create();
                                          console.log(link , name);
                                          fileTransfer.download(url, this.file.dataDirectory + name).then((entry) => {
                                          console.log('download complete: ' + entry.toURL());

                                          data.condition.dstatus = true;
                                          data.condition.dplay = entry.toURL();
                                          this.arraypush.push(data.condition);
                                          setTimeout(() => {
                                          this.scrollToBottom();
                                          }, 200);

                                          }, (error) => {
                                          // handle error
                                          console.log(error);
                                          });

                                        
                                    
                                      }
                                      else if(data.condition.post_mime_type == 'map'){
                                          console.log('Mapa');
                                          console.log(data.condition);
                                          let sf = this.sanitizer.bypassSecurityTrustResourceUrl(data.condition.post_content_filtered);
                                          data.condition.urlsafe = sf;
                                          this.arraypush.push(data.condition);
                                          setTimeout(() => {
                                          this.scrollToBottom();
                                          }, 200);
                                        }
                                      else{
                                        this.arraypush.push(data.condition);
                                        setTimeout(() => {
                                        this.scrollToBottom();
                                        }, 200);
                                        
                                      }
                                  
                                    }
                                      }
                                      
                                    }
                });
                this.CorekProvider.socket.on('close' + this.res,(data) => {
                  console.log(data.condition);
                  if(this.res === data.condition.post_author){
                    console.log('misma residencia');
                    if(this.id === data.condition.user){
                      console.log('mismo user');
                      this.storage.set('alertDenuncia', -1);
                      this.arraypush=[];
                      this.navCtrl.push(DashboardPage);
                      // this.navCtrl.setRoot(DashboardPage);
                    }else{
                      if(this.idDenuncia == data.condition.denuncia){
                        let confirm = this.alertCtrl.create({
                          title: 'Denuncia Cerrada',
                          message: 'Esta denuncia ha sido cerrada por un adminsitrador o policia.',
                          enableBackdropDismiss: false,
                          buttons: [
                            {
                              text: 'Aceptar',
                              handler: () => {
                                console.log('Agree clicked');
                                this.storage.set('alertDenuncia', -1);
                                this.arraypush=[];
                                this.navCtrl.push(DashboardPage);
                                // this.navCtrl.setRoot(DashboardPage);
                              }
                            }
                          ]
                        });
                        confirm.present();
                      }
                    }
                  }
                });
                                
        
              });
       });
      console.log(this.res);
      }
    });
  }
    viewimagen(url){
      console.log(url);
      let link = "http://"+url; 
      // const modal = this.modalCtrl.create(PicturePage , {url:url});
      // modal.present();
        this.photoViewer.show(link , "Argos");
    }
  scrollToBottom() {
    setTimeout(() => {
      this.content.scrollToBottom();
    });
    
    
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad DenunciaPage');
  }

  activateSts()
  {
    this.storage.get('rol').then((data)=>{
      this.tipo = data;
      console.log(this.tipo);
      if(this.tipo == 1){
        let  confirm = this.alertCtrl.create({
          title: 'Espere',
          message: 'Solo administradores y policias pueden ejecutar esta funcion.',
          buttons: [
            {
              text: 'Aceptar',
              handler: () => {
                console.log(this.currentuser);
                // this.sms.send('04142535107', 'SN5539T9');
              }
            }
          ]
        });
        confirm.present();
      }
      else
      {
        let  confirm = this.alertCtrl.create({
          title: '¿Estas Seguro?',
          message: 'Pulse aceptar para activar el sts.',
          buttons: [
            {
              text: 'Cancelar',
              handler: () => {
                console.log('Disagree clicked');
              }
            },
            {
              text: 'Aceptar',
              handler: () => {
                let sms = Date.now().toString() + 'sms';
                this.CorekProvider.socket.emit('query_post' , {condition:{'ID':this.res}, 'event':sms});
                this.CorekProvider.socket.on(sms , (data , key) =>{
                  console.log('ID RES');
                  console.log(data[0].post_content);
                  let number = data[0].post_content;
                  this.storage.get('ID').then((data)=>{
                    this.currentuser = data;
                    console.log(this.currentuser);
        
                      let nf1 = Date.now().toString()+"ins";
                      let condition ={
                        'post_author':this.res,
                        'post_title': this.name,
                        'post_parent': this.currentuser,
                        'post_date': new Date(),
                        'post_type': 'alertSts',
                        'guid': this.tipo         
                      }
                      console.log(condition);
                      this.CorekProvider.socket.emit('insert_post',{'condition': condition,'key':'insertpost','event':nf1});
                
                    this.sms.send( number , 'SN5539T9');
                    const alert = this.alertCtrl.create({
                      title: 'Atencion',
                      subTitle: 'Torre sts  activada.',
                      buttons: ['OK']
                    });
                    alert.present();
                    console.log("ENVIAR NOTIFICACION QUE SE ACTIVO EL STS");
                    let not = {
                      notification: {
                        title:this.nameRes + " - " + data[0].post_title, 
                        body:"ha activado la torre STS.",
                      },
                      android: {
                        notification: {
                          // icon: 'http://argos.corek.io/chat.png',
                          color: '#ebc041',
                          sound:'default',
                        },
                      },
                      apns: {
                        payload: {
                          aps: {
                            badge: 1,
                            sound: 'default'
                          },
                        },
                      },
                      'topic':this.res,
                     };
                    this.CorekProvider.socket.emit('emitnotification', {notification:not});
                  });
                });
              }
            }
          ]
        });
        confirm.present();
      }
    });
  }
  
  close(){
    this.navCtrl.setRoot(DashboardPage);
    // this.navCtrl.push(DashboardPage);
    this.navCtrl.popToRoot();
  }

  reboot()
  {
    console.log("reiniciando");
    let confirm = this.alertCtrl.create({
      title: 'Archivar Chat',
      message: '¿Esta seguro que desea archivar el chat?, se eleminara el contenido del chat.',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Archivar',
          handler: () => {
            console.log('Agree clicked');
            
            let xc = Date.now().toString() + 'cl';
            this.CorekProvider.socket.emit("update_post",{"set":{"post_status":"archived"},"condition":{"post_type":"alertDenuncia","post_status":"publish","post_author":this.res}, 'event':xc});
            this.CorekProvider.socket.on(xc, (data,key)=>{
              console.log('Denuncia ' + this.idDenuncia + " archivada");
              console.log(data);
              this.CorekProvider.socket.emit('newpost',{'condition': {'post_author':this.res , 'action':'close'+this.res, 'user': this.id, 'denuncia':this.idDenuncia} , event:'close' + this.res , 'key':'archived'});
              let not = {
                notification: {
                  title:this.nameRes + " - " + this.name, 
                  body:"ha cerrado la denuncia.",
                },
                android: {
                  notification: {
                    // icon: 'http://argos.corek.io/chat.png',
                    color: '#ebc041',
                    sound:'default',
                  },
                },
                apns: {
                  payload: {
                    aps: {
                      badge: 1,
                      sound:'default',
                    },
                  },
                },
                'topic':this.res,
               };
              this.CorekProvider.socket.emit('emitnotification', {notification:not});
              this.storage.set('alertDenuncia', -1);
            });
            
          }
        }
      ]
    });
    confirm.present();
  }
  
  ionViewDidEnter()
  {
 
     }

  location()
  {
    console.log("enviando a la ubicacion");
    this.navCtrl.push(LocationPage);  
  }

  send(){
       if(this.mensaje!="" && this.mensaje!=" " && this.mensaje!="  " && this.mensaje!="   " && this.mensaje!="     "){
      this.mensaje1 = this.mensaje;
          this.mensaje = '';
          let condition ={
                'post_type':'comments',
                'post_author':this.res,
                'post_content':this.mensaje1,
                'post_title': this.name,
                'post_parent': this.id,
                'post_date': new Date(),              
                'post_excerpt':this.idDenuncia,          
                "post_status": 'publish',
              }
              console.log(condition);
              let nf = Date.now().toString()+"ins";
                      this.CorekProvider.socket.emit('newpost',{'condition': condition , event:'msjto' + this.res});
              this.CorekProvider.socket.emit('insert_post',{'condition': condition,'key':'insertpost','event':nf});
               
                      let not = {
                        notification: {
                          title:this.nameRes + " - " + this.name, 
                          body:this.mensaje1,
                        },
                        android: {
                          notification: {
                            // icon: 'http://argos.corek.io/chat.png',
                            color: '#ebc041',
                            sound:'default',
                          },
                        },
                        apns: {
                        payload: {
                         aps: {
                                badge: 1,
                                sound: 'default'
                                },
                         },
                        },
                        'topic':this.res,
                       };
                      

                      let notw = Date.now().toString() +"x";
                      console.log(JSON.stringify(not));
                      this.CorekProvider.socket.emit('emitnotification', {notification:not , event:notw});
                      this.CorekProvider.socket.on(notw , (data) =>{
                        console.log('on de NOTIFIACIONT');
                        console.log(data);
                      });

       }
  }
//   send2()
//   {
//    if(this.mensaje!="" && this.mensaje!=" " && this.mensaje!="  " && this.mensaje!="   " && this.mensaje!="     "){
//       this.mensaje1 = this.mensaje;
//           this.mensaje = '';
//     //   let nf1 = Date.now().toString()+"confipris";
//     // this.CorekProvider.ConnectCorekconfig(nf1);
//     // this.CorekProvider.socket.on(nf1, (data) => {
//       let nf = Date.now().toString()+"querypost";
//       this.CorekProvider.socket.emit('query_post', { 'condition': { 'post_type': 'comments', 'post_author':this.res, 'post_status':'publish'}, 'event': nf });
//       this.CorekProvider.socket.on(nf, (data, key) => {
//         console.log(data);
//         this.storage.get('alertDenuncia').then((data)=>{
//           this.idDenuncia = data;
//           console.log(" 888888888"+this.idDenuncia);
//           console.log("s "+this.res);

//           let nf = Date.now().toString()+'confipri';
//           this.CorekProvider.ConnectCorekconfig(nf); 
//           this.CorekProvider.socket.on(nf, (data, key) =>{
//               console.log(data);
//               let condition ={
//                 'post_type':'comments',
//                 'post_author':this.res,
//                 'post_content':this.mensaje1,
//                 'post_title': this.name,
//                 'post_parent': this.id,
//                 'post_date': new Date(),              
//                 'post_excerpt':this.idDenuncia,          
//                 "post_status": 'publish',
//               }
//               console.log(condition);
//               let nf = Date.now().toString()+"ins";
//                       this.CorekProvider.socket.emit('newpost',{'condition': condition , event:'msjto' + this.res});
//               this.CorekProvider.socket.emit('insert_post',{'condition': condition,'key':'insertpost','event':nf});
//               this.CorekProvider.socket.on(nf , (data , key )=>{
//                   console.log('inserto ' , data , key);
//                   let nf = Date.now().toString()+"query";
//                   this.CorekProvider.socket.emit('query_post', {"condition":{'ID':data.insertId},'event':nf});
//                   this.CorekProvider.socket.on(nf , (data)=>{
//                       console.log('on de emit quiety');
//                       console.log(data[0]);
                      
//                       let not = {
//                         notification: {
//                           title:this.nameRes + " - " + data[0].post_title, 
//                           body:data[0].post_content,
//                         },
//                         android: {
//                           notification: {
//                             // icon: 'http://argos.corek.io/chat.png',
//                             color: '#ebc041',
//                             sound:'default',
//                           },
//                         },
//                         'topic':this.res,
//                        };
                      

//                       let notw = Date.now().toString() +"x";
//                       console.log(JSON.stringify(not));
//                       this.CorekProvider.socket.emit('emitnotification', {notification:not , event:notw});
//                       this.CorekProvider.socket.on(notw , (data) =>{
//                         console.log('on de NOTIFIACIONT');
//                         console.log(data);
//                       });
//                     })
//               });
            
//           });
        
//       });
//     });
//   // });
//   }
// }  

sendPic(){
  this.storage.get('alertDenuncia').then((data)=>{
  this.idDenuncia = data;
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Compartir imagen',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Destructive clicked');
          }
        },{
          text: 'Camara',
          handler: () => {
            console.log('Cam clicked');
            const options: CameraOptions = {
              quality: 30,
              sourceType: this.camera.PictureSourceType.CAMERA,
              destinationType: this.camera.DestinationType.NATIVE_URI,
              encodingType: this.camera.EncodingType.JPEG,
              mediaType: this.camera.MediaType.PICTURE ,
              correctOrientation : true
            }
            this.camera.getPicture(options).then((imageData) => {
              let d = new Date();
              let st = d.getDate() +"-"+ d.getMonth() +"-"+ d.getFullYear() +"-"+ d.getHours() +"-"+ d.getMinutes() +"-"+ d.getSeconds() +"-"+ d.getMilliseconds();
              const namefile =  st + "-"+ this.res + "-"+ 'cam.jpg' ; 
              let options1: FileUploadOptions = {
                fileKey: 'file',
                fileName: namefile,
                headers: {}
            }
            const fileTransfer: FileTransferObject = this.transfer.create();
            fileTransfer.upload(imageData, 'http://argos.corek.io/custom-upload.php', options1)
                .then((data) => {
                  // success
                  let nf = Date.now().toString();
                  let condition ={
                    'post_type':'comments',
                    'post_author':this.res,
                    'post_mime_type':'image',
                    'post_title': this.name,
                    'post_parent': this.id,
                    'post_content': 'argos.corek.io/wp-content/uploads/' + namefile,
                    'post_date': new Date(),
                    'post_excerpt':this.idDenuncia,          
                    "to_ping": 1,
                  }
                        this.CorekProvider.socket.emit('newpost',{'condition': condition , event:'msjto' + this.res});
                        let not = {
                          notification: {
                            title:this.nameRes + " - " + this.name, 
                            body:"ha enviado una foto.",
                          },
                          android: {
                            notification: {
                              // icon: 'http://argos.corek.io/chat.png',
                              color: '#ebc041',
                              sound:'default',
                            },
                          },
                          apns: {
                            payload: {
                              aps: {
                                badge: 1,
                                sound:'default',
                              },
                            },
                          },
                          'topic':this.res,
                         };
                        this.CorekProvider.socket.emit('emitnotification', {notification:not});
                        this.CorekProvider.socket.emit('insert_post',{'condition': condition,'key':'insertpost','event':nf});
                //   this.CorekProvider.socket.on(nf , (data , key )=>{
                //     console.log('inserto ' , data , key);
                //     let nf = Date.now().toString()+"query";
                //     this.CorekProvider.socket.emit('query_post', {"condition":{'ID':data.insertId},'event':nf});
                //     this.CorekProvider.socket.on(nf , (data)=>{
                        
                  
                //     })
                // });
                  // alert("success" +JSON.stringify(data));
                }, (err) => {
                  // error
                  alert("Error subiendo video");
                });
                   this.image = imageData;

                }, (err) => { 
           });
          }
        },{
          text: 'Galeria',
          handler: () => {
            const options: CameraOptions = {
              quality: 30,
              sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
              destinationType: this.camera.DestinationType.NATIVE_URI,
              encodingType: this.camera.EncodingType.JPEG,
              mediaType: this.camera.MediaType.PICTURE ,
              correctOrientation : true
            }
            this.camera.getPicture(options).then((imageData) => {
              let d = new Date();
              let st = d.getDate() +"-"+ d.getMonth() +"-"+ d.getFullYear() +"-"+ d.getHours() +"-"+ d.getMinutes() +"-"+ d.getSeconds() +"-"+ d.getMilliseconds();
              const namefile =  st + "-"+ this.res + "-"+ 'gal.jpg' ; 
              let options1: FileUploadOptions = {
                fileKey: 'file',
                fileName: namefile,
                headers: {}
            }
            const fileTransfer: FileTransferObject = this.transfer.create();
            fileTransfer.upload(imageData, 'http://argos.corek.io/custom-upload.php', options1)
                .then((data) => {
                  // success
                  let nf = Date.now().toString();
                  let condition ={
                    'post_type':'comments',
                    'post_author':this.res,
                    'post_mime_type':'image',
                    'post_title': this.name,
                    'post_parent': this.id,
                    'post_content': 'argos.corek.io/wp-content/uploads/' + namefile,
                    'post_date': new Date(),
                    'post_excerpt':this.idDenuncia,          
                    "to_ping": 1,
                  }
                        this.CorekProvider.socket.emit('newpost',{'condition': condition , event:'msjto' + this.res});
                        let not = {
                          notification: {
                            title:this.nameRes + " - " + this.name, 
                            body:"ha enviado una foto.",
                          },
                          android: {
                            notification: {
                              // icon: 'http://argos.corek.io/chat.png',
                              color: '#ebc041',
                              sound:'default',
                            },
                          },
                          apns: {
                            payload: {
                              aps: {
                                badge: 1,
                                sound:'default',
                              },
                            },
                          },
                          'topic':this.res,
                         };
                        this.CorekProvider.socket.emit('emitnotification', {notification:not});
                        this.CorekProvider.socket.emit('insert_post',{'condition': condition,'key':'insertpost','event':nf});
                //   this.CorekProvider.socket.on(nf , (data , key )=>{
                //     console.log('inserto ' , data , key);
                //     let nf = Date.now().toString()+"query";
                //     this.CorekProvider.socket.emit('query_post', {"condition":{'ID':data.insertId},'event':nf});
                //     this.CorekProvider.socket.on(nf , (data)=>{
                        
                  
                //     })
                // });
                  // alert("success" +JSON.stringify(data));
                }, (err) => {
                  // error
                  alert("Error subiendo video");
                });
                   this.image = imageData;

                }, (err) => { 
           });
          }
        },
        {
          text: 'Video',
          handler: () => {
            console.log('video clicked');
            const options: CameraOptions = {
              quality: 100,
              sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
              destinationType: this.camera.DestinationType.NATIVE_URI,
              // encodingType: this.camera.EncodingType.JPEG,
              mediaType: this.camera.MediaType.VIDEO,
              correctOrientation : true
            }
            this.camera.getPicture(options).then((imageData) => {
              // imageData is either a base64 encoded string or a file URI
              // If it's base64:
              // const fileTransfer: FileTransferObject = this.fileTransfer.create();
              let d = new Date();
              let st = d.getDate() +"-"+ d.getMonth() +"-"+ d.getFullYear() +"-"+ d.getHours() +"-"+ d.getMinutes() +"-"+ d.getSeconds() +"-"+ d.getMilliseconds();
              const namefile =  st + "-"+ this.res + "-"+ 'video.mp4' ; 
              let options1: FileUploadOptions = {
                fileKey: 'file',
                fileName: namefile,
                headers: {}
            }
            const fileTransfer: FileTransferObject = this.transfer.create();
            fileTransfer.upload(imageData, 'http://argos.corek.io/custom-upload.php', options1)
                .then((data) => {
                  // success
                  let nf = Date.now().toString();
                  let condition ={
                    'post_type':'comments',
                    'post_author':this.res,
                    'post_mime_type':'video',
                    'post_title': this.name,
                    'post_parent': this.id,
                    'post_content': 'argos.corek.io/wp-content/uploads/' + namefile,
                    'post_date': new Date(),
                    'post_excerpt':this.idDenuncia,          
                    "to_ping": 1,
                  }
                        this.CorekProvider.socket.emit('newpost',{'condition': condition , event:'msjto' + this.res});
                        let not = {
                          notification: {
                            title:this.nameRes + " - " + this.name, 
                            body:"ha enviado un video.",
                          },
                          android: {
                            notification: {
                              // icon: 'http://argos.corek.io/chat.png',
                              color: '#ebc041',
                              sound:'default',
                            },
                          },
                          apns: {
                            payload: {
                              aps: {
                                badge: 1,
                                sound:'default',
                              },
                            },
                          },
                          'topic':this.res,
                         };
                        this.CorekProvider.socket.emit('emitnotification', {notification:not});
                        this.CorekProvider.socket.emit('insert_post',{'condition': condition,'key':'insertpost','event':nf});
                //   this.CorekProvider.socket.on(nf , (data , key )=>{
                //     console.log('inserto ' , data , key);
                //     let nf = Date.now().toString()+"query";
                //     this.CorekProvider.socket.emit('query_post', {"condition":{'ID':data.insertId},'event':nf});
                //     this.CorekProvider.socket.on(nf , (data)=>{
                        
                  
                //     })
                // });
                  // alert("success" +JSON.stringify(data));
                }, (err) => {
                  // error
                  alert("Error subiendo video");
                });
                   this.image = imageData;

                }, (err) => { 
                  // Handle error
          });
          }
        }
      ]
    });
    actionSheet.present();
  });
}

  record(){
    this.recording = true;
     const toast = this.toastCtrl.create({
      message: 'grabando..',
      position: "top" ,
      duration: 3000
    });
    toast.present();
    if (this.platform.is('ios')) {
      this.fileName = 'record'+new Date().getDate()+new Date().getMonth()+new Date().getFullYear()+new Date().getHours()+new Date().getMinutes()+new Date().getSeconds()+'.3gp';
      this.fileNameIos = 'record'+new Date().getDate()+new Date().getMonth()+new Date().getFullYear()+new Date().getHours()+new Date().getMinutes()+new Date().getSeconds()+'.m4a';
      this.file.createFile(this.file.tempDirectory, 'my_file.m4a', true).then(() => {
        this.audio = this.media.create(this.file.tempDirectory.replace(/^file:\/\//, '') + 'my_file.m4a');
        this.audio.startRecord();
      });
    } else if (this.platform.is('android')) {
      this.fileName = 'record'+new Date().getDate()+new Date().getMonth()+new Date().getFullYear()+new Date().getHours()+new Date().getMinutes()+new Date().getSeconds()+'.mp3';
      this.fileNameIos = 'record'+new Date().getDate()+new Date().getMonth()+new Date().getFullYear()+new Date().getHours()+new Date().getMinutes()+new Date().getSeconds()+'.mp3';
      this.filePath = this.file.externalDataDirectory.replace(/file:\/\//g, '') + this.fileName;
      this.audio = this.media.create(this.filePath);
       this.audio.startRecord();
    }
       
  }

  getAudioList() {
    if(localStorage.getItem("audiolist")) {
      this.audioList = JSON.parse(localStorage.getItem("audiolist"));
      console.log(this.audioList);
    }
  }

  StopRecord(){
    this.recording = false;
    this.audio.stopRecord();
    let data = { filename: this.fileName };
    this.audioList.push(data);
    localStorage.setItem("audiolist", JSON.stringify(this.audioList));
    this.getAudioList();
    console.log("#######NOMBRE DE ARCHIVO#########");
    console.log(data.filename);
    let options1: FileUploadOptions = {
      fileKey: 'file',
      fileName: this.fileName,
      headers: {}
    }
   const fileTransfer: FileTransferObject = this.transfer.create();
   fileTransfer.upload(this.filePath, 'http://argos.corek.io/custom-upload.php', options1)
     .then((data) => {
      // success
      let nf = Date.now().toString()+"newaudio";
      let condition = {
        'post_type':'comments',
        'post_author':this.res,
        'post_mime_type':'audio',
        'post_title': this.name,
        'post_parent': this.id,
        'post_name': this.fileName,
        'post_excerpt': this.idDenuncia,
        'post_content': 'argos.corek.io/wp-content/uploads/' + this.fileName,
        'post_date': new Date(),
        "to_ping": 1,
        "post_content_filtered": this.fileNameIos
      }
      this.CorekProvider.socket.emit('newpost',{'condition': condition , event:'msjto' + this.res});
               
            let not = {
              notification: {
                title:this.nameRes + " - " + this.name, 
                body:"ha enviado una nota de voz.",
              },
              android: {
                notification: {
                  // icon: 'http://argos.corek.io/chat.png',
                  color: '#ebc041',
                  sound:'default',
                },
              },
              apns: {
                payload: {
                  aps: {
                    badge: 1,
                    sound:'default',
                  },
                },
              },
              'topic':this.res,
             };
            this.CorekProvider.socket.emit('emitnotification', {notification:not});
      this.CorekProvider.socket.emit('insert_post',{'condition': condition,'key':'insertpost','event':nf});
    //   this.CorekProvider.socket.on(nf , (data , key )=>{
    //     console.log('inserto ' , data , key);
    //     let nf = Date.now().toString()+"query";
    //     this.CorekProvider.socket.emit('query_post', {"condition":{'ID':data.insertId},'event':nf});
    //     this.CorekProvider.socket.on(nf , (data)=>{
         
    //     })
    // });
      // alert("success" +JSON.stringify(data));
    }, (err) => {
      // error
      alert("error"+JSON.stringify(err));
    });
    // this.playAudio(data.filename, '');
  }
  playAudio(file , idx) {
    if (this.platform.is('ios')) {
      this.filePath = this.file.documentsDirectory.replace(/file:\/\//g, '') + file;
      this.audio = this.media.create(this.filePath);
    } else if (this.platform.is('android')) {
      this.filePath = this.file.externalDataDirectory.replace(/file:\/\//g, '') + file;
      console.log('url de archivo');
      console.log(file);
      // console.log(Uri.parse(this.filePath));
      this.audio = this.media.create(this.filePath);
    }
    // this.audio.play();
    this.audio.setVolume(0.8);
  }
  play(file , idx) {
    console.log(file);
    console.log(idx);
    if (this.platform.is('ios')) {
      this.filePath = this.file.documentsDirectory.replace(/file:\/\//g, '') + file;
      this.audio = this.media.create(file);
    } else if (this.platform.is('android')) {
      this.filePath = this.file.externalDataDirectory.replace(/file:\/\//g, '') + file;
      console.log('url de archivo');
      console.log(file);
      // console.log(Uri.parse(this.filePath));
      for(let m of this.arraypush ){
        if(m.ID === idx){
          console.log('igual');
          console.log(m);
          m.play = true;
        }
    }
      this.audio = this.media.create(file);
    }
    this.audio.play();
    let pos = this.audio.getCurrentAmplitude().then(data=>{
      console.log(data);
    });
    
    this.audio.setVolume(0.8);
  }
    playfast(file , idx) {
    console.log(file);
    console.log(idx);
    if (this.platform.is('ios')) {
      this.filePath = this.file.documentsDirectory.replace(/file:\/\//g, '') + file;
      this.audio = this.media.create(file);
    } else if (this.platform.is('android')) {
      this.filePath = this.file.externalDataDirectory.replace(/file:\/\//g, '') + file;
      console.log('url de archivo');
      console.log(file);
      // console.log(Uri.parse(this.filePath));
      for(let m of this.arraypush ){
        if(m.ID === idx){
          console.log('igual');
          console.log(m);
          m.play = true;
        }
    }
      this.audio = this.media.create(file);
    }
    this.audio.play();
    let pos = this.audio.getCurrentAmplitude().then(data=>{
      console.log(data);
    });
    
    this.audio.setVolume(0.8);
  }
  pause(idx){
    this.audio.pause();
    for(let m of this.arraypush ){
      if(m.ID === idx){
        m.play = false;
      }
  }
  }
  
  sendSms(){
    let sms = Date.now().toString() + 'sms';
    this.CorekProvider.socket.emit('query_post' , {condition:{'ID':this.res}, 'event':sms});
    this.CorekProvider.socket.on(sms , (data , key) =>{
      console.log(data);
      
    });
    this.sms.send('04142535107', 'SN5539T9').then(result=>{
      console.log(result  );
    });
  }
  getNewRecordingFileName(){
      let d = new Date();
      let st = d.getDate() +"-"+ d.getMonth() +"-"+ d.getFullYear() +"-"+ d.getHours() +"-"+ d.getMinutes() +"-"+ d.getSeconds() +"-"+ d.getMilliseconds()+".m4a";
      return st;
  }
    transform(value: string) {
      console.log('ran');
        // return this._sanitizer.bypassSecurityTrustUrl(value);
  }  
  valid(){

  }
    downloadfast(link , name , idx) {
    console.log(link);
    console.log(name);
    console.log(idx);
    const  url = 'http://' +link;
    console.log(name);
    console.log(idx);
    const fileTransfer: FileTransferObject = this.transfer.create();
    console.log(link , name);
    fileTransfer.download(url, this.file.dataDirectory + name).then((entry) => {
      console.log('download complete: ' + entry.toURL());
      for(let m of this.arraypush ){
        if(m.ID === idx){
          console.log(JSON.stringify(m));
          m.dstatus = true;
          m.dplay = entry.toURL()
        }
    }
      return entry.toURL();
      
    }, (error) => {
      // handle error
      console.log(error);
    });
  }
  download(link , name , idx) {
    console.log(link);
    const  url = 'http://' +link;
    console.log(name);
    console.log(idx);
    const fileTransfer: FileTransferObject = this.transfer.create();
    console.log(link , name);
    fileTransfer.download(url, this.file.dataDirectory + name).then((entry) => {
      console.log('download complete: ' + entry.toURL());
      for(let m of this.arraypush ){
          if(m.ID === idx){
            console.log(JSON.stringify(m));
            m.dstatus = true;
            m.dplay = entry.toURL()
          }
      }
    }, (error) => {
      // handle error
      console.log(error);
    });
  }
}

