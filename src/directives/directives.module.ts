import { NgModule } from '@angular/core';
import { QuickPressDirective } from './quick-press/quick-press';
@NgModule({
	declarations: [QuickPressDirective],
	imports: [],
	exports: [QuickPressDirective]
})
export class DirectivesModule {}
