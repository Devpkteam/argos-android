import { Directive, ElementRef, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core'
import { Gesture } from 'ionic-angular/gestures/gesture'
declare var Hammer;

/**
 * Generated class for the QuickPressDirective directive.
 *
 * See https://angular.io/api/core/Directive for more info on Angular
 * Directives.
 */
@Directive({
  selector: '[quick-press]' // Attribute selector
})
export class QuickPressDirective implements OnInit, OnDestroy {
  el: HTMLElement;
  pressGesture: Gesture;
  @Output('quickPress') onPress: EventEmitter<any> = new EventEmitter();
  @Output('quickPressUp') onPressUp: EventEmitter<any> = new EventEmitter();
  constructor(el: ElementRef)  {
    console.log('Hello QuickPressDirective Directive');
    this.el = el.nativeElement;
  }
  ngOnInit() {
    const pressOptions = {
      recognizers: [
        [Hammer.Press, { time: 100 }]
      ]
    }

    this.pressGesture = new Gesture(this.el, pressOptions);
    this.pressGesture.listen();

    this.pressGesture.on('pressup', e => {
      this.onPressUp.emit(e)
    })

    this.pressGesture.on('press', e => {
      console.log('press');
      this.onPress.emit(e);
    })
  }
  ngOnDestroy() {
    this.pressGesture.destroy();
  }
}
