import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { CorekProvider } from '../providers/corek/corek';
import { IonicStorageModule } from '@ionic/storage';
import { LocationPage } from '../pages/location/location';
import { LongPressModule } from 'ionic-long-press';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { RegistroPage } from '../pages/registro/registro';
import { LoginPage } from '../pages/login/login';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { DenunciaPage } from '../pages/denuncia/denuncia';
import { PermisosPage } from '../pages/permisos/permisos';
import { Camera } from '@ionic-native/camera';
import { Media} from '@ionic-native/media';
import { File } from '@ionic-native/file';
import { SMS } from '@ionic-native/sms';
import { QuickPressDirective } from  '../directives/quick-press/quick-press'
import { FileTransfer } from '@ionic-native/file-transfer';
import { Device } from '@ionic-native/device';
import { Geolocation } from '@ionic-native/geolocation';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { FCM } from "@ionic-native/fcm";
import { Network } from '@ionic-native/network';
import { PhotoViewer } from '@ionic-native/photo-viewer';
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    RegistroPage,
    LoginPage,
    DashboardPage,
    DenunciaPage,
    QuickPressDirective,
    LocationPage,
    PermisosPage
  ],
  imports: [
    BrowserModule,
    IonicStorageModule.forRoot({
      name: '__argos',
         driverOrder: ['sqlite', 'websql']
    }),
    IonicModule.forRoot(MyApp ,{
      backButtonText: ''
    }),
    LongPressModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    RegistroPage,
    LoginPage,
    DashboardPage,
    DenunciaPage,
    LocationPage,
    PermisosPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    CorekProvider,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Camera,
    Media,
    File,
    SMS,
    FileTransfer,
    Device,
    Geolocation,
    LocationAccuracy,
    AndroidPermissions,
    FCM,
    Network,
    PhotoViewer
  ]
})
export class AppModule {}
