import { DenunciaPage } from './../pages/denuncia/denuncia';
import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';
import { HomePage } from '../pages/home/home';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { Network } from '@ionic-native/network';  
import { LoadingController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any;
  
  constructor(public storage : Storage,platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen , private network: Network , public loadingCtrl: LoadingController , public toastCtrl: ToastController) {
    const loader = this.loadingCtrl.create({
      content: "No tiene conexion a internet."
    });
    this.network.onDisconnect().subscribe(() => {
      console.log('network was disconnected :-(');
      loader.present();
    });
    this.network.onConnect().subscribe(() => {
      console.log('network connected!');
      loader.dismiss();
      setTimeout(() => {
        if (this.network.type === 'wifi') {
          console.log('we got a wifi connection, woohoo!');
        }
      }, 3000);
    });
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();   
    });
    this.rootPage =DenunciaPage;


  }
}

